<?php

namespace App\Form;

use App\Entity\Address;
use App\Entity\Carrier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $options['user']; //recupérer précisément l'user à travers le tableau $options
        $builder
            ->add('addresses', EntityType::class, [
                'label' => false,
                'required' => true,
                'class'=> Address::class,
                'multiple'=> false,
                'expanded' => true,
                'choices' => $user->getAddresses()
            ])
            ->add('carriers', EntityType::class, [
                'label' => "Choisissez votre transporteur",
                'required' => true,
                'class'=> Carrier::class,
                'multiple'=> false,
                'expanded' => true
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Valider",
                'attr' => [
                    'class'=>'btn-block btn-info'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
            'user'=> array() // recuperer l'user qui a été passé au niveau du OrderController
        ]);
    }
}
