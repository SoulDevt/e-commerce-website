<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AccountPasswordController extends AbstractController
{
    private $entityManager;

    /**
     * @param $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/compte/modifier-mon-mot-de-passe", name="account_password")
     */
    public function index(Request $request, UserPasswordHasherInterface $encoder): Response
    {
        $notification = null;
        $user = $this->getUser(); // chopper l'utilisateur avec ses infos déjà connecté
        $form = $this->createForm(ChangePasswordType::class, $user); // creer le formulaire à partir du form type
        $form->handleRequest($request); // écouter la validation du formulaire
        if($form->isSubmitted() and $form->isValid()) { // tester si le formulaire est soumis et valide
            $old_pwd = $form->get('old_password')->getData(); //chopper le mot de passe actuel(ancien)
            if($encoder->isPasswordValid($user,$old_pwd)) {
                $new_pwd = $form->get('new_password')->getData();
                $password= $encoder->hashPassword($user, $new_pwd);
                $user->setPassword($password);
                $this->entityManager->persist($user); //pas obliger dans le cas d'une mise à jour
                $this->entityManager->flush();
                $notification= "Votre mot de passe a été mis à jour";
            } else {
                $notification= "Votre mot de passe actuel n'est pas le bon";
            }
        }

        return $this->render('account/password.html.twig', [
            'form'=> $form->createView(),
            'notification' => $notification
        ]);
    }
}
